package NaverLiveRank;

import java.io.*;
import java.net.*;
import java.util.*;

public class WebCrawling {

	public static void main(String[] args) {
		String url ="https://www.naver.com";
		HttpURLConnection con =null;
		BufferedReader br = null;
		FileWriter fw = null;
		while(true) {
			try {
				con = (HttpURLConnection) (new URL(url)).openConnection();
				con.connect();
				InputStreamReader isr = new InputStreamReader(con.getInputStream(),"UTF-8");
				br = new BufferedReader(isr);
				Date time = new Date();
				String fileName = (time.getYear()+1900)+"년"+(time.getMonth()+1)+"월"+
						+time.getDate()+"일"+time.getHours()+"시"+time.getMinutes()+"분"+
						+time.getSeconds()+"초의 실시간 검색어.txt";
				fw = new FileWriter(fileName);

				String temp;
				int count = 0;
				while((temp= br.readLine()) != null) {
					if(temp.contains("<span class=\"ah_r\">")) {
						fw.write(temp.split(">")[1].split("<")[0] + "위 : ");
					}
					if(temp.contains("<span class=\"ah_k\">")) {
						fw.write(temp.split(">")[1].split("<")[0]);
						fw.write("\r\n");
						count++;
					}
					if(count==20) break;
				}

			} catch(IOException e) {
				e.printStackTrace();
			} finally {
				if(br!=null) {try {br.close();} catch(IOException e) {System.out.println(e.getMessage());}}
				if(fw!=null) {try {fw.close();} catch(IOException e) {System.out.println(e.getMessage());}}
				
				if(con!=null) {con.disconnect();}
			}
			try {
				System.out.println("검색어가 저장되었습니다.");
				Thread.sleep(60000);
				System.out.println("다시 검색어를 가져옵니다.");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}//end while
	}
}